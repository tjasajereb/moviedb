import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Movie from '../views/Movie.vue'
import TV from '../views/TV.vue'
import Actor from '../views/Actor.vue'
import Results from '../views/Results.vue'
import PopularMovies from '../views/PopularMovies.vue'
import NowPlaying from '../views/NowPlaying.vue'
import Upcoming from '../views/Upcoming.vue'
import TopRated from '../views/TopRated.vue'
import SignupScreen from '../views/SignupScreen.vue'
import LoginScreen from '../views/LoginScreen.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
    {
      path: '/register',
      name: 'signupScreen',
      component: SignupScreen

    },
    {
      path: '/login',
      name: 'loginScreen',
      component: LoginScreen

    },
  {
    path: '/popular-movies',
    name: 'popularMovies',
    component: PopularMovies
  },
    {
      path: '/now-playing',
      name: 'nowPlaying',
      component: NowPlaying
    },
    {
      path: '/upcoming',
      name: 'upcoming',
      component: Upcoming
    },
    {
      path: '/top-rated',
      name: 'toprated',
      component: TopRated
    },

  {
    path: '/movie/:movieId',
    name: 'Movie',
    component: Movie
  },
  {
    path: '/tv/:tvId',
    name: 'TV',
    component: TV
  },
  {
    path: '/result/:resultQuery',
    name: 'Results',
    component: Results
  },
  {
    path: '/actor/:actorId',
    name: 'Actor',
    component: Actor
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
