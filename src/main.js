// Import
import Vue from 'vue'
import Popular from './components/Popular/Popular.vue'
import Trending from './components/Trending/Trending.vue'
import Menu from './components/Menu/Menu.vue'
import Search from './components/Search/Search.vue'
import Footer from './components/Footer/Footer.vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css' //Vuesax styles
import 'material-icons/iconfont/material-icons.css'
import Vuelidate from 'vuelidate'
import Signup from './components/auth/Signup.vue'
import Login from './components/auth/Login.vue'
import * as firebase from "firebase";
import store from "./store";




Vue.use(VueAxios, axios)
Vue.config.productionTip = false
Vue.use(Vuesax)
Vue.use(Vuelidate)


const configOptions = {
  apiKey: "AIzaSyD5-J4n5_o0ACwUo-gENx5LEmFSZAk0e-8",
  authDomain: "moviedatabase1995.firebaseapp.com",
  databaseURL: "https://moviedatabase1995.firebaseio.com",
  projectId: "moviedatabase1995",
  storageBucket: "moviedatabase1995.appspot.com",
  messagingSenderId: "1048342696668",
  appId: "1:1048342696668:web:826a964bc2621ee38ada19"
};

firebase.initializeApp(configOptions);

firebase.auth().onAuthStateChanged(user => {
  store.dispatch("fetchUser", user);
});


// Declare components
Vue.component('popular', Popular);
Vue.component('trending', Trending);
Vue.component('menu', Menu);
Vue.component('search', Search);
Vue.component('footer', Footer);
Vue.component('signup', Signup);
Vue.component('login', Login);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

